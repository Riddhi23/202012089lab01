# 202012089 LAB01-EC Poll Simulator <br />
<br />
The project is a lab exercise to implement MVC and make students understand how to differentiate Business Logic from Presentation Logic. <br />
1. Model (Data-Layer) <br />
-> I have created a separate file which will handle all the operations related to Data Access and Data Modification/Manipulation. The file can be found with the name of data.js under datalayer folder.<br />
2. View (Presentation-Layer)<br />
-> The folder concerned with the view of the project have been separated by adding the files to another folder.
You can find the files under views folder. <br />
3. Controller (Request and Response Manipulation) <br />
-> The project has been separated under two controllers. One is admin and another is user. Admin can access the functionality to add candidate records and to view the results. Whereas, user can cast the vote only once to any desired candidate. <br />
Check out the project at: https://lab1-202012089.herokuapp.com/menu <br />




